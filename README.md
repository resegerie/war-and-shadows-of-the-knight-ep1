### CodinGame: Solutions pour WAR & Shadows of the Knight EP1

Vous trouverez ici:
- Les solutions en _C_ du jeu **WAR** (La Bataille) et **Shadows of the Knight EP1**.
- Le fichier **ORG** et **HTML** du rapport.
- Des fichiers annexes utilisés par le fichier ORG maître (**.png** et **.txt**).

### CodinGame: Solutions for WAR & Shadows of the Knight EP1

Here you will find:
- The _C_ solutions of the game **WAR** (La Bataille) and **Shadows of the Knight EP1**.
- The **ORG** and **HTML** file of the report.
- Additional files used by the master ORG file (**.png** and **.txt**).
